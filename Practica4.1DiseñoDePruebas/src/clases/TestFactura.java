package clases;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class TestFactura {

	Factura factura;
	
	static GestorContabilidad gestorPruebas;
	
	@Before
	public void CrearFacturaPrevioTest() {
		factura = new Factura();
		factura.setPrecioUnidad(2.5F);
		factura.setCantidad(3);
	}
	
	/*
	 * Buscamos una factura que no existe en la listaFacturas con el codigo factura
	 */
	@Test
	public void testBuscarFacturaInexistente() {
		String codigoFactura = "23543dfs";
		Factura actual = gestorPruebas.buscarFactura(codigoFactura);
		assertNull(actual);
	}
	
	/*
	 * Buscamos una factura que no existe en la listaFacturas, como hay mas facturas usamos el codigo factura
	 */
	@Test
	public void testBuscarFacturaInexistenteHabiendoFacturas() {
		Factura facturaNueva = new Factura();
		facturaNueva.setCodigoFactura("12344rfg");
		gestorPruebas.listaFacturas.add(facturaNueva);
		
		String codigoFactura = "83323gf";
		Factura actual = gestorPruebas.buscarFactura(codigoFactura);
		assertNull(actual);
	}
	
	/*
	 * Buscamos una factura existente en la listaFacturas con el codigo factura
	 */
	@Test 
	public void testBuscarFacturaExistente() {
		Factura facturaNueva = new Factura();
		facturaNueva.setCodigoFactura("12344rfg");
		gestorPruebas.listaFacturas.add(facturaNueva);
		
		String codigoFactura = "233rfreg";
		Factura actual = gestorPruebas.buscarFactura(codigoFactura);
		assertSame(facturaNueva, actual);
	}

	/*
	 * Buscamos una factura existente en la listaFacturas, como hay mas facturas usamos el codigo factura 
	 * que es unico en cada factura
	 */
	@Test
	public void testBuscarFacturaConVariasExistentes() {
		Factura factura1 = new Factura();
		factura1.setCodigoFactura("12344rfg");
		gestorPruebas.listaFacturas.add(factura1);
		
		Factura factura2 = new Factura();
		factura1.setCodigoFactura("03421vcz");
		gestorPruebas.listaFacturas.add(factura2);
		
		Factura actual = gestorPruebas.buscarFactura("2354676J");
		assertSame(factura2, actual);
	}

	/*
	 * Añadimos una factura nueva a la listaFacturas, le metemos como unico dato el codigo ya que es lo que nos interesa
	 */
	@Test
	public void testAñadirNuevaFacturaPorCodigo() {
		Factura nuevaFactura = new Factura();
		nuevaFactura.setCodigoFactura("34214583KJ");
		gestorPruebas.listaFacturas.add(nuevaFactura);
		assertNull(nuevaFactura);
	}
	
	/*
	 * Añadimos una factura nueva a la listaFacturas, pero al tener otra factura comparamos sus codigos
	 */
	@Test
	public void testComprobarCodigoHabiendoOtraFactura() {
		
		Factura factura = new Factura("42619072H", LocalDate.now(), "Velas", 1.5F, 3, null);
		gestorPruebas.listaFacturas.add(factura);
		
		Factura nuevaFactura = new Factura("79644092H", LocalDate.now(), "Velas", 1.5F, 3, null);
		assertSame(factura, nuevaFactura);
	}
	
	/*
	 * Añadimos una factura nueva a la listaFacturas,como tenemos varias facturas el codigo de la factura nos 
	 * perimitara saber si esa factura ya esta introducida o no
	 */
	@Test
	public void testComprobarCodigoHabiendoMasFacturas() {
		
		Factura factura1 = new Factura("4316987G", LocalDate.now(), "zapatos", 10.5F, 4, null);
		gestorPruebas.listaFacturas.add(factura1);
		
		Factura factura2 = new Factura("79644092H", LocalDate.now(), "zapatos", 10.5F, 6, null);
		gestorPruebas.listaFacturas.add(factura2);
		
		Factura nuevaFactura = new Factura("79644092H", LocalDate.now(), "zapatos", 1.5F, 3, null);
		assertNull(nuevaFactura);
	}

	/*
	 * Eliminamos una factura de la listaFacturas pasandole el codigo de la factura
	 *  ya que por cada factura hay un codigo unico
	 */
	@Test 
	public void testEliminarFactura() {
		Factura factura = new Factura("2213243Z", LocalDate.now(), "gorras", 4F, 5, null);
		gestorPruebas.listaFacturas.add(factura);
		
		String codigoFactura = "2213243Z";
		factura = gestorPruebas.buscarFactura(codigoFactura);
		gestorPruebas.listaFacturas.remove(factura);
		assertNull(factura);
	}

	/*
	 * Eliminamos una factura de la listaFacturas pasandole el codigo ya que es el unico identificador unico
	 * para cada factura, ya que dado el caso de que dos facturas tengan exactamente los mismos datos lo unico 
	 * que nos ayuda a saber cual borrar es su codigo y en todo caso su cliente asociado ya que el cliente tambien tiene
	 * un DNI que es unico por persona
	 */
	@Test
	public void testEliminarFacuraHabiendoMasFacturas() {
		Factura factura1 = new Factura("00482193A", LocalDate.now(), "lamparas", 20.5F, 7, null);
		gestorPruebas.listaFacturas.add(factura1);
		
		Factura factura2 = new Factura("38273243M", LocalDate.now(), "alfombras", 35.5F, 4, null);
		gestorPruebas.listaFacturas.add(factura2);
		
		String codigoFactura = "38273243M";
		Factura actual = gestorPruebas.buscarFactura(codigoFactura);
		factura2 = actual;
		gestorPruebas.listaFacturas.remove(actual);
		assertEquals(codigoFactura,factura1, actual);
		
	}
}
