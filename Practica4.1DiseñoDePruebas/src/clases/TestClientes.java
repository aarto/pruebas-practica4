package clases;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestClientes {

	static GestorContabilidad gestorPruebas;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		gestorPruebas = new GestorContabilidad();
	}

	@Before
	public void setUp() throws Exception {
	}

	/*
	 * Buscamos un cliente que no existe en la listaClientes mediante el dni
	 */
	@Test
	public void testBuscarClienteInexistente() {
		String dni = "1232834";
		Cliente actual = gestorPruebas.buscarCliente(dni);
		assertNull(actual);
	}
	
	/*
	 * Buscamos un cliente que no existe en la listaClientes mediante el dni, teniendo otros clientes a�adidos
	 */
	@Test
	public void testBuscarClienteInexistenteHabiendoClientes() {
		Cliente clienteNuevo = new Cliente("1234R", "Alba", LocalDate.now());
		gestorPruebas.listaClientes.add(clienteNuevo);
		
		String dni = "1232834";
		Cliente actual = gestorPruebas.buscarCliente(dni);
		assertNull(actual);
	}
	
	/*
	 * Buscamos un cliente que ya existe en la listaClientes mediante el dni
	 */
	@Test 
	public void testBuscarClienteExistente() {
		Cliente clienteNuevo = new Cliente("1234R", "Alba", LocalDate.now());
		gestorPruebas.listaClientes.add(clienteNuevo);
		
		Cliente actual = gestorPruebas.buscarCliente("1234R");
		assertSame(clienteNuevo, actual);
	}

	/*
	 * Buscamos un cliente que ya existe en la listaClientes mediante el dni, teniendo ya otros clientes a�adidos
	 */
	@Test
	public void testBuscarClienteConVariosExistentes() {
		Cliente clienteJuan = new Cliente("3532545H", "Juan", LocalDate.now());
		gestorPruebas.listaClientes.add(clienteJuan);
		
		Cliente clienteLaura = new Cliente("2354676J", "Laura", LocalDate.now());
		gestorPruebas.listaClientes.add(clienteLaura);
		
		Cliente actual = gestorPruebas.buscarCliente("2354676J");
		assertSame(clienteLaura, actual);
	}
	
	/*
	 * A�adimos un cliente nuevo a la listaClientes, podriamos a�adirlo con todos sus atributos
	 * pero nos interesa el DNI, ya que es unico en cada persona
	 */
	@Test
	public void testA�adirNuevoClientePorDNI() {
		Cliente nuevoCliente = new Cliente("45233722L");
		gestorPruebas.altaCliente(nuevoCliente);	
	}
	
	/*
	 * A�adimos un cliente nuevo a la listaClientes teniendo ya otro cliente, asi que al agregar el nuevo 
	 * cliente lo hacemos con el dni, asi nos aseguramos de que no hay otro
	 */
	@Test
	public void testComprobarDNIConOtroCliente() {
		
		Cliente clienteNuevo = new Cliente("42619072H", "Raul");
		gestorPruebas.listaClientes.add(clienteNuevo);
		
		String dni = "45233722L";
		Cliente actual = gestorPruebas.buscarCliente(dni);
		assertNull(actual);
	}
	
	/*
	 * Lo mismo que el amterior, a�adimos un cliente nuevo a la listaClientes teniendo ya otro cliente, asi que al agregar el nuevo 
	 * cliente lo hacemos con el dni, asi nos aseguramos de que no hay otro
	 * Pero en este caso tenemos otro cliente mas en la listaClientes
	 */
	@Test
	public void testComprobarDNIConMasClientes() {
		
		Cliente cliente1 = new Cliente("3532545H", "Jorge");
		gestorPruebas.listaClientes.add(cliente1);
		
		Cliente cliente2= new Cliente("2354676J", "Jorge");
		gestorPruebas.listaClientes.add(cliente2);
		
		String dni = "45233722L";
		Cliente actual = gestorPruebas.buscarCliente(dni);
		assertSame(cliente1, actual);
	}
	
	/*
	 * Buscamos el cliente mas antiguo de la listaClientes
	 */
	@Test
	public void testClienteConMasAntiguedad() {
		Cliente clienteNuevo = new Cliente("36721076J", "Ana", LocalDate.now());
		gestorPruebas.listaClientes.add(clienteNuevo);
	
		Cliente masAntiguo = gestorPruebas.clienteMasAntiguo();
		assertNull(masAntiguo);
	}
	
	/*
	 * Buscamos al cliente mas antiguo de la listaClientes dandole una fecha anterior a la actual
	 */
	@Test
	public void testClienteMasAntiguoSiHayOtroCliente() {
		Cliente clienteNuevo = new Cliente("36721076J", "Ana", LocalDate.now());
		gestorPruebas.listaClientes.add(clienteNuevo);
		
		LocalDate fecha = LocalDate.of(2015, 8, 15); 
		
		Cliente antiguo = new Cliente();
		antiguo.setFechaAlta(fecha);
		antiguo = gestorPruebas.clienteMasAntiguo();
		assertNull(antiguo);
	}
	
	/*
	 * Buscamos al cliente mas antiguo de la listaClientes dandole una fecha anterior a la actual, 
	 * en este caso seria lo mismo pero con un cliente mas y las fechas de antiguedad estan desordenadas
	 */
	@Test
	public void testClienteMasAntiguoSiHayVariosClientes() {
		Cliente clienteFelix = new Cliente("25206753R", "Felix", LocalDate.now());
		gestorPruebas.listaClientes.add(clienteFelix);
		
		LocalDate fecha = LocalDate.of(2015, 8, 15); 
		Cliente clienteSebastian = new Cliente("25206753R", "Sebastian", fecha);
		gestorPruebas.listaClientes.add(clienteSebastian);
		
		LocalDate fecha2 = LocalDate.of(2016, 10, 23); 
		Cliente clienteHugo = new Cliente();
		clienteHugo.setFechaAlta(fecha2);
		clienteHugo = gestorPruebas.clienteMasAntiguo();
		assertSame(clienteSebastian, clienteHugo);
	}
		
	/*
	 * Eliminamos un cliente de la listaClientes pasandole el dni ya que es el unico identificador unico
	 * para cada persona
	 */
	@Test 
	public void testEliminarCliente() {
		Cliente cliente1 = new Cliente("23435414V", "Mark", LocalDate.now());
		gestorPruebas.listaClientes.add(cliente1);
		
		String dni = "23435414V";
		cliente1 = gestorPruebas.buscarCliente(dni);
		gestorPruebas.listaClientes.remove(cliente1);
		assertNull(cliente1);
	}

	/*
	 * Eliminamos un cliente de la listaClientes pasandole el dni ya que es el unico identificador unico
	 * para cada persona, ya que dado el caso de que dos clientes tengan exactamente los mismos datos lo unico 
	 * que nos ayuda a saber cual borrar es su DNI
	 */
	@Test
	public void testEliminarClienteConVariosClientes() {
		Cliente clienteJuan = new Cliente("3532545H", "Juan", LocalDate.now());
		gestorPruebas.listaClientes.add(clienteJuan);
		
		Cliente clienteLaura = new Cliente("2354676J", "Laura", LocalDate.now());
		gestorPruebas.listaClientes.add(clienteLaura);
		
		String dni = "23435414V";
		Cliente actual = gestorPruebas.buscarCliente(dni);
		gestorPruebas.listaClientes.remove(actual);
		assertEquals(clienteLaura, actual);
	}
}
