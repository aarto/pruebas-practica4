package clases;

import java.util.ArrayList;

public class GestorContabilidad{
	
	public ArrayList<Factura>listaFacturas;
	public ArrayList<Cliente>listaClientes;
	
	public GestorContabilidad() {
		listaFacturas = new ArrayList<>();
		listaClientes = new ArrayList<>();	
	}
	
	public Cliente buscarCliente(String dni) {
		
		if(listaClientes.size()>0) {
			
			return listaClientes.get(0);
		}
		
		return null;
	}

	public Factura buscarFactura(String condigo) {
		
		if(listaFacturas.size()>0) {
			
			return listaFacturas.get(0);
		}
		
		return null;
	}
	
	public void altaCliente(Cliente cliente) {
		
		if(!listaClientes.contains(cliente)) {
			
			listaClientes.add(cliente);
		}
	}
	
	public void crearFactura(Factura factura) {
		
		if(!listaFacturas.contains(factura)) {
			
			listaFacturas.add(factura);
		}
	}
	
	public Cliente clienteMasAntiguo() {
		
		Cliente clienteAntiguo = listaClientes.get(0);
		
		if(clienteAntiguo != null) {
			
			for(Cliente cliente : listaClientes) {
				
				if(clienteAntiguo.getFechaAlta().isBefore(cliente.getFechaAlta())) {
					
					clienteAntiguo = cliente;
				}
			}
		} 
		
		else {
			
			return null;
		}
		
		return clienteAntiguo;
	}
	
	public Factura facturaMasCara() {
		
		Factura facturaCara = listaFacturas.get(0);
		
		if(facturaCara != null) {
			
			for(Factura factura : listaFacturas) {
				
				if(facturaCara.calcularPrecioTotal() < factura.calcularPrecioTotal()) {
					
					facturaCara = factura;
				}
			}
		} 
		
		else {
			
			return null;
		}
		
		return facturaCara;
	}
	
	public double calcularFacturacionAnual(int anno) {
		
		double facturacionAnual = 0;
		
		for(Factura factura : listaFacturas) {
			if(factura.getFecha().getYear() == anno) {
				facturacionAnual += factura.calcularPrecioTotal();
			}
		}
		return facturacionAnual;
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		
		Factura factura = buscarFactura(codigoFactura);
		
		Cliente cliente = buscarCliente(dni);
		
		if(factura != null && cliente != null) {
			
			factura.setCliente(cliente);
		}
	}
	
	public int cantidadFacturasPorCliente(String dni) {
		
		Cliente cliente = buscarCliente(dni);
		
		int numeroFacturas = 0;
		
		for(Factura factura : listaFacturas) {
			
			if(factura.getCliente().equals(cliente)) {
				numeroFacturas++;
			}
		}
		
		return numeroFacturas;
	}
	
	public void eliminarFactura(String codigoFactura) {
		
		listaFacturas.remove(buscarFactura(codigoFactura));
	}
	
	public void eliminarCliente(String dni) {
		
		listaClientes.remove(buscarCliente(dni));
	}
}